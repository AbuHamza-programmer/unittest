#include<iostream>
#include<fstream>
#include<string>
#include"unittest.h"


void test1()
{
	UnitTest unit_test1; 
	//UnitTest unit_test1(std::cout, false); //constructor

	unit_test1.detailed(true); //display detailed report

	unit_test1.startUnit("Testing operator+");//start a unit

	unit_test1.test("1+1==2" , 1+1==2);
	unit_test1.test("1+2==3" , 1+2==3);
	unit_test1.test("1+3==-1" , 1+3==-1);//wrong

	unit_test1.unitReport();//unit report

	// std::ofstream out("output.txt");
	// unit_test1.redirectTo(out); //redirect output to file
	

	unit_test1.startUnit("Testing operator<");//new unit

	unit_test1.test("1<1" , 1<1);
	unit_test1.test("1<=1" , 1<=1);
	unit_test1.test("1>2" , 1>2);//wrong

	unit_test1.unitReport();//unit report

	unit_test1.finalReport();//full report 

}

int main()
{
	
	test1();
	

	return 0;
}