# README #

Simple unit testing aplication, in one file.

### Features ###

* Start different units in the same object.
* Reports for each unit and for all units.
* Detailed and brief reports.
* Ability to redirect output.

